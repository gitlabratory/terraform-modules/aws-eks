output "cluster_id" {
  value = module.eks-cluster.cluster_id
}

output "node_group_arn" {
  value = module.node-group.node_group_arn
}
