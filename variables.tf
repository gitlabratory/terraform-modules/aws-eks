variable "aws_region" {
  description = "AWS region"
  default     = "us-east-1"
}

variable "cluster_name" {
  description = "EKS Cluster name"
  default     = "my-cluster"
}

variable "node_group_name" {
  description = "EKS Node Group name"
  default     = "my-node-group"
}
