provider "aws" {
  region = var.aws_region
}

module "vpc" {
  source = "git::https://gitlab.com/your-repo/aws-vpc.git"
  aws_region = var.aws_region
}

module "eks-cluster" {
  source = "./modules/eks-cluster"
  cluster_name = var.cluster_name
  vpc_id = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets
}

module "node-group" {
  source = "./modules/node-group"
  cluster_name = var.cluster_name
  node_group_name = var.node_group_name
  subnet_ids = module.vpc.private_subnets
}
