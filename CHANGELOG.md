## [1.0.2](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/compare/1.0.1...1.0.2) (2023-09-26)


### Bug Fixes

* **ci:** tf module creation ([4019db8](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/4019db866bf40dd33f5e47b57fe6323627a5f896))
* **ci:** tf module creation ([9777a39](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/9777a392347c079b2bc672f6cef6479e1870ede6))

## [1.0.1](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/compare/1.0.0...1.0.1) (2023-09-25)


### Bug Fixes

* **ci:** adding terraform template ([f94c9b3](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/f94c9b3511579ff27803fd2b0553175791027c0c))

# 1.0.0 (2023-09-25)


### Features

* create aws-eks module ([2bbb1f2](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/2bbb1f20a9a46e4756411d5258bda3c2aead352a))
* create aws-eks module ([d10eec1](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/d10eec1998f40d091601f642e33f880bfef52042))
* create aws-eks module ([03798e9](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/03798e93fd6982c52729a4fa31f540d9d7cfe30e))
* create aws-eks module ([6ef0a73](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/6ef0a73969af4d2387c03b95034c754ab1b00875))
* create aws-eks module ([88d8ab2](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/88d8ab2aff3afe6cebf8443773b234bf1733212f))
* create aws-eks module ([f527301](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/f5273013961f096a4cde43bb76a38eac9ffdb6be))
* create aws-eks module ([674e85a](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/674e85ac1b2d8892381749bab7269998c3e29e9b))
* create aws-vpc module ([ac76571](https://gitlab.com/gitlabratory/terraform-modules/aws-eks/commit/ac76571be6af1f83869b44c86b791cc9a0e4619a))
