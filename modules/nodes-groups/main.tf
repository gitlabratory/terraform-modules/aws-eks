variable "cluster_name" {}
variable "node_group_name" {}
variable "subnet_ids" {}

resource "aws_eks_node_group" "this" {
  cluster_name = var.cluster_name
  node_group_name = var.node_group_name
  subnet_ids = var.subnet_ids
}

output "node_group_arn" {
  value = aws_eks_node_group.this.arn
}
