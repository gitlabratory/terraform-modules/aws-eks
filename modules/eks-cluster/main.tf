variable "cluster_name" {}
variable "vpc_id" {}
variable "subnet_ids" {}

resource "aws_eks_cluster" "this" {
  name = var.cluster_name
  role_arn = aws_iam_role.eks_cluster.arn
  vpc_config {
    subnet_ids = var.subnet_ids
  }
}

output "cluster_id" {
  value = aws_eks_cluster.this.id
}
